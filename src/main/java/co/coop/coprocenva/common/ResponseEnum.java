/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.coop.coprocenva.common;

/**
 *
 * @author Stiven Lenis Cardona <Stivenlenis@hotmail.com>
 */
public enum ResponseEnum {
    
     //Respuesta generica
    CODE_SUCCESS,
    
    //Request
    REQUEST_EXIST
    
}

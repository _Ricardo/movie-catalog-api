package co.coop.coprocenva.common.error;

import co.coop.coprocenva.common.ResponseEnum;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Clase manejadora de errores
 */
public class CoreException extends Exception
{
    
    private final ResponseEnum error;
    private final List<Object> paramsList;

    public ResponseEnum getError()
    {
        return error;
    }

    public List<Object> getParams()
    {
        return paramsList;
    }

    public CoreException(ResponseEnum errormessage)
    {
        this.paramsList = new ArrayList<>();
        error = errormessage;
    }

    public CoreException(ResponseEnum errormessage, Throwable cause)
    {
        super(errormessage.name(), cause);
        this.paramsList = new ArrayList<>();
        error = errormessage;
    }

    public CoreException(ResponseEnum errormessage, Throwable cause, Object... params)
    {
        super(errormessage.name(), cause);
        this.paramsList = new ArrayList<>();
        paramsList.addAll(Arrays.asList(params));
        error = errormessage;
    }
    
    public CoreException(ResponseEnum errormessage, Object... params)
    {
        this.paramsList = new ArrayList<>();
        paramsList.addAll(Arrays.asList(params));
        error = errormessage;
    }
}

package co.coop.coprocenva.servicedesk.service;

import co.coop.coprocenva.common.ResponseEnum;
import co.coop.coprocenva.common.error.CoreException;
import co.coop.coprocenva.servicedesk.model.PersonType;
import co.coop.coprocenva.servicedesk.repository.IPersonTypeRepository;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonTypeService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final IPersonTypeRepository personTypeRepository;

    @Autowired
    public PersonTypeService(IPersonTypeRepository personTypeRepository) {
        this.personTypeRepository = personTypeRepository;
    }

    @Transactional()
    public void savePersonType(PersonType personType) throws CoreException {
        if (personType.getId() == null) {
            personTypeRepository.save(personType);
        } else {
            logger.error("Se intento guardar un registro con id");
            throw new CoreException(ResponseEnum.REQUEST_EXIST);
        }
    }

    @Transactional()
    public void updatePersonType(PersonType personType) throws CoreException {
        try {
            personTypeRepository.save(personType);
        } catch (Exception e) {
            logger.error("Ocurrio un error al momento de guardar el registro", e);
            throw new CoreException(ResponseEnum.REQUEST_EXIST);
        }
    }

    @Transactional(readOnly = true)
    public List<PersonType> getAllPersonTypes() throws CoreException {
        return personTypeRepository.findAll();
    }

    @Transactional()
    public void deletePersonType(Long id) throws CoreException {
        try {
            personTypeRepository.deleteById(id);
        } catch (Exception e) {
            logger.error("Ocurrio un error al momento de guardar el registro", e);
            throw new CoreException(ResponseEnum.REQUEST_EXIST);
        }
    }
}

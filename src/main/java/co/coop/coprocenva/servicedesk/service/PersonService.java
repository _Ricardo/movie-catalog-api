package co.coop.coprocenva.servicedesk.service;

import co.coop.coprocenva.common.ResponseEnum;
import co.coop.coprocenva.common.error.CoreException;
import co.coop.coprocenva.servicedesk.model.Person;
import co.coop.coprocenva.servicedesk.repository.IPersonRepository;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final IPersonRepository personRepository;

    @Autowired
    public PersonService(IPersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Transactional()
    public void savePerson(Person person) throws CoreException {
        if (person.getId() == null) {
            personRepository.save(person);
        } else {
            logger.error("Se intento guardar un registro con id");
            throw new CoreException(ResponseEnum.REQUEST_EXIST);
        }
    }

    @Transactional()
    public void updatePerson(Person person) throws CoreException {
        try {
            personRepository.save(person);
        } catch (Exception e) {
            logger.error("Ocurrio un error al momento de guardar el registro", e);
            throw new CoreException(ResponseEnum.REQUEST_EXIST);
        }
    }

    @Transactional(readOnly = true)
    public List<Person> getAllPerson() throws CoreException {
        return personRepository.findAll();
    }

    @Transactional()
    public void deletePerson(Long id) throws CoreException {
        try {
            personRepository.deleteById(id);
        } catch (Exception e) {
            logger.error("Ocurrio un error al momento de guardar el registro", e);
            throw new CoreException(ResponseEnum.REQUEST_EXIST);
        }
    }
}

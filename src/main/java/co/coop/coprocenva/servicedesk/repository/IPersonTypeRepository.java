package co.coop.coprocenva.servicedesk.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.coop.coprocenva.servicedesk.model.PersonType;

@Repository
public interface IPersonTypeRepository extends JpaRepository<PersonType, Long> {

}

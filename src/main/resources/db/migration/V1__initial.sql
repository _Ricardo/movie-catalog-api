-- -----------------------------------------------------
-- Schema coprocenva
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Table coprocenva.states
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "states" (
  state_id INT NOT NULL AUTO_INCREMENT,
  type VARCHAR(4) NOT NULL,
  name VARCHAR(45) NOT NULL,
  description VARCHAR(200) NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NULL DEFAULT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (state_id))
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table coprocenva.departments
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "departments" (
  department_id INT NOT NULL AUTO_INCREMENT,
  country_id INT NOT NULL,
  code VARCHAR(3) NOT NULL,
  name VARCHAR(100) NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (department_id),
  UNIQUE INDEX code_UNIQUE (code ASC) VISIBLE,
  INDEX fk_departments_countries_idx (country_id ASC) VISIBLE,
  INDEX fk_departments_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_departments_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_departments_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_departments_countries
    FOREIGN KEY (country_id)
    REFERENCES coprocenva.countries (country_id),
  CONSTRAINT fk_departments_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_departments_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_departments_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.dependencies
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "dependencies" (
  dependence_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(200) NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (dependence_id),
  UNIQUE INDEX name_UNIQUE (name ASC) VISIBLE,
  INDEX fk_dependencies_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_dependencies_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_dependencies_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_dependencies_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_dependencies_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_dependencies_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.offices
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "offices" (
  office_id INT NOT NULL AUTO_INCREMENT,
  code VARCHAR(3) NOT NULL,
  name VARCHAR(100) NOT NULL,
  dependence_id INT NOT NULL,
  country_id INT NOT NULL,
  department_id INT NOT NULL,
  city_id INT NOT NULL,
  address VARCHAR(60) NOT NULL,
  state_id INT NOT NULL,
  create_date DATETIME NULL DEFAULT NULL,
  create_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (office_id),
  UNIQUE INDEX code_UNIQUE (code ASC) VISIBLE,
  INDEX fk_offices_dependencies1_idx (dependence_id ASC) VISIBLE,
  INDEX fk_offices_countries1_idx (country_id ASC) VISIBLE,
  INDEX fk_offices_departments1_idx (department_id ASC) VISIBLE,
  INDEX fk_offices_Cities1_idx (city_id ASC) VISIBLE,
  INDEX fk_offices_users1_idx (create_user_id ASC) VISIBLE,
  INDEX fk_offices_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_offices_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_offices_Cities1
    FOREIGN KEY (city_id)
    REFERENCES coprocenva.Cities (city_id),
  CONSTRAINT fk_offices_countries1
    FOREIGN KEY (country_id)
    REFERENCES coprocenva.countries (country_id),
  CONSTRAINT fk_offices_departments1
    FOREIGN KEY (department_id)
    REFERENCES coprocenva.departments (department_id),
  CONSTRAINT fk_offices_dependencies1
    FOREIGN KEY (dependence_id)
    REFERENCES coprocenva.dependencies (dependence_id),
  CONSTRAINT fk_offices_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_offices_users1
    FOREIGN KEY (create_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_offices_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.users
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "users" (
  user_id INT NOT NULL AUTO_INCREMENT,
  document VARCHAR(11) NOT NULL,
  date_expedition DATETIME NOT NULL,
  user VARCHAR(10) NOT NULL,
  password VARCHAR(100) NULL DEFAULT NULL,
  name VARCHAR(100) NOT NULL,
  mail VARCHAR(100) NOT NULL,
  main_rol_id INT NULL DEFAULT NULL,
  office_id INT NOT NULL,
  first_login BIT(1) NOT NULL,
  state_id INT NOT NULL,
  create_date DATETIME NULL DEFAULT NULL,
  create_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (user_id),
  UNIQUE INDEX document_UNIQUE (document ASC) VISIBLE,
  UNIQUE INDEX user_UNIQUE (user ASC) VISIBLE,
  INDEX fk_users_offices1_idx (office_id ASC) VISIBLE,
  INDEX fk_users_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_users_offices1
    FOREIGN KEY (office_id)
    REFERENCES coprocenva.offices (office_id),
  CONSTRAINT fk_users_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id));


-- -----------------------------------------------------
-- Table coprocenva.countries
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "countries" (
  country_id INT NOT NULL AUTO_INCREMENT,
  code VARCHAR(3) NOT NULL,
  name VARCHAR(100) NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NULL DEFAULT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (country_id),
  UNIQUE INDEX code_UNIQUE (code ASC) VISIBLE,
  INDEX fk_countries_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_countries_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_countries_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_countries_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_countries_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_countries_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.Cities
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "cities" (
  city_id INT NOT NULL AUTO_INCREMENT,
  country_id INT NOT NULL,
  department_id INT NOT NULL,
  code VARCHAR(3) NOT NULL,
  name VARCHAR(100) NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NOT NULL,
  creation_user_id INT NOT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NOT NULL,
  PRIMARY KEY (city_id),
  UNIQUE INDEX code_UNIQUE (code ASC) VISIBLE,
  INDEX fk_Cities_countries1_idx (country_id ASC) VISIBLE,
  INDEX fk_Cities_departments1_idx (department_id ASC) VISIBLE,
  INDEX fk_Cities_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_Cities_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_Cities_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_Cities_countries1
    FOREIGN KEY (country_id)
    REFERENCES coprocenva.countries (country_id),
  CONSTRAINT fk_Cities_departments1
    FOREIGN KEY (department_id)
    REFERENCES coprocenva.departments (department_id),
  CONSTRAINT fk_Cities_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_Cities_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_Cities_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.attention_times
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "attention_times" (
  attention_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NOT NULL,
  value INT NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NOT NULL,
  creation_user_id INT NOT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NOT NULL,
  PRIMARY KEY (attention_id),
  UNIQUE INDEX name_UNIQUE (name ASC) VISIBLE,
  INDEX fk_attention_times_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_attention_times_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_attention_times_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_attention_times_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_attention_times_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_attention_times_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.levels
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "levels" (
  level_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NOT NULL,
  description VARCHAR(45) NOT NULL,
  value INT NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NOT NULL,
  PRIMARY KEY (level_id),
  UNIQUE INDEX name_UNIQUE (name ASC) VISIBLE,
  INDEX fk_levels_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_levels_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_levels_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_levels_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_levels_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_levels_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.roles
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "roles" (
  rol_id INT NOT NULL AUTO_INCREMENT,
  code VARCHAR(15) NOT NULL,
  name VARCHAR(45) NOT NULL,
  description VARCHAR(100) NOT NULL,
  level_id INT NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NOT NULL,
  PRIMARY KEY (rol_id),
  UNIQUE INDEX code_UNIQUE (code ASC) VISIBLE,
  INDEX fk_roles_levels1_idx (level_id ASC) VISIBLE,
  INDEX fk_roles_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_roles_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_roles_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_roles_levels1
    FOREIGN KEY (level_id)
    REFERENCES coprocenva.levels (level_id),
  CONSTRAINT fk_roles_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_roles_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_roles_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.briefcase
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "briefcase" (
  briefcase_id INT NOT NULL AUTO_INCREMENT,
  code VARCHAR(15) NOT NULL,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(200) NOT NULL,
  owner_rol_id INT NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NOT NULL,
  creation_user_id INT NOT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NOT NULL,
  PRIMARY KEY (briefcase_id),
  UNIQUE INDEX code_UNIQUE (code ASC) VISIBLE,
  INDEX fk_briefcase_roles1_idx (owner_rol_id ASC) VISIBLE,
  INDEX fk_briefcase_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_briefcase_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_briefcase_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_briefcase_roles1
    FOREIGN KEY (owner_rol_id)
    REFERENCES coprocenva.roles (rol_id),
  CONSTRAINT fk_briefcase_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_briefcase_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_briefcase_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.services
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "services" (
  service_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  description VARCHAR(200) NOT NULL,
  attention_id INT NULL DEFAULT NULL,
  breafcase_id INT NULL DEFAULT NULL,
  supplier BIT(1) NOT NULL,
  qualifying BIT(1) NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (service_id),
  UNIQUE INDEX name_UNIQUE (name ASC) VISIBLE,
  INDEX fk_services_briefcase1_idx (breafcase_id ASC) VISIBLE,
  INDEX fk_services_attention_times1_idx (attention_id ASC) VISIBLE,
  INDEX fk_services_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_services_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_services_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_services_attention_times1
    FOREIGN KEY (attention_id)
    REFERENCES coprocenva.attention_times (attention_id),
  CONSTRAINT fk_services_briefcase1
    FOREIGN KEY (breafcase_id)
    REFERENCES coprocenva.briefcase (briefcase_id),
  CONSTRAINT fk_services_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_services_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_services_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.requests
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "requests" (
  request_id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(100) NOT NULL,
  office_id INT NOT NULL,
  service_id INT NOT NULL,
  description TEXT NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NULL DEFAULT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (request_id),
  INDEX fk_requests_services1_idx (service_id ASC) VISIBLE,
  INDEX fk_requests_offices1_idx (office_id ASC) VISIBLE,
  INDEX fk_requests_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_requests_offices1
    FOREIGN KEY (office_id)
    REFERENCES coprocenva.offices (office_id),
  CONSTRAINT fk_requests_services1
    FOREIGN KEY (service_id)
    REFERENCES coprocenva.services (service_id),
  CONSTRAINT fk_requests_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id));


-- -----------------------------------------------------
-- Table coprocenva.attachments
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "attachments" (
  attached_id INT NOT NULL,
  request_id INT NULL DEFAULT NULL,
  attached_name VARCHAR(100) NULL DEFAULT NULL,
  attached_url VARCHAR(200) NULL DEFAULT NULL,
  state_id INT NULL DEFAULT NULL,
  creation_date DATETIME NOT NULL,
  creation_user_id INT NOT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NOT NULL,
  PRIMARY KEY (attached_id),
  INDEX fk_attached_documents_requests1_idx (request_id ASC) VISIBLE,
  INDEX fk_attached_documents_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_attached_documents_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_attachments_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_attached_documents_requests1
    FOREIGN KEY (request_id)
    REFERENCES coprocenva.requests (request_id),
  CONSTRAINT fk_attached_documents_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_attached_documents_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_attachments_state
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id));


-- -----------------------------------------------------
-- Table coprocenva.menus
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "menus" (
  menu_id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NOT NULL,
  description VARCHAR(100) NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NOT NULL,
  update_user_id INT NOT NULL,
  PRIMARY KEY (menu_id),
  UNIQUE INDEX name_UNIQUE (name ASC) VISIBLE,
  INDEX fk_menus_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_menus_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_menus_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_menus_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_menus_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_menus_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.landings
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "landings" (
  landing_id INT NOT NULL AUTO_INCREMENT,
  menu_id INT NULL DEFAULT NULL,
  name VARCHAR(45) NULL DEFAULT NULL,
  url VARCHAR(200) NULL DEFAULT NULL,
  description VARCHAR(100) NULL DEFAULT NULL,
  state_id INT NULL DEFAULT NULL,
  creation_date DATETIME NULL DEFAULT NULL,
  creation_user_id INT NULL DEFAULT NULL,
  update_date DATETIME NULL DEFAULT NULL,
  update_user_id INT NULL DEFAULT NULL,
  PRIMARY KEY (landing_id),
  INDEX fk_landings_menus1_idx (menu_id ASC) VISIBLE,
  INDEX fk_landings_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_landings_users2_idx (update_user_id ASC) VISIBLE,
  INDEX fk_landings_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_landings_menus1
    FOREIGN KEY (menu_id)
    REFERENCES coprocenva.menus (menu_id),
  CONSTRAINT fk_landings_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_landings_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id),
  CONSTRAINT fk_landings_users2
    FOREIGN KEY (update_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.responses
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "responses" (
  response_id INT NOT NULL AUTO_INCREMENT,
  request_id INT NOT NULL,
  description LONGTEXT NOT NULL,
  state_id INT NOT NULL,
  creation_date DATETIME NOT NULL,
  creation_user_id INT NOT NULL,
  PRIMARY KEY (response_id),
  INDEX fk_responses_requests1_idx (request_id ASC) VISIBLE,
  INDEX fk_responses_users1_idx (creation_user_id ASC) VISIBLE,
  INDEX fk_responses_status1_idx (state_id ASC) VISIBLE,
  CONSTRAINT fk_responses_requests1
    FOREIGN KEY (request_id)
    REFERENCES coprocenva.requests (request_id),
  CONSTRAINT fk_responses_status1
    FOREIGN KEY (state_id)
    REFERENCES coprocenva.states (state_id),
  CONSTRAINT fk_responses_users1
    FOREIGN KEY (creation_user_id)
    REFERENCES coprocenva.users (user_id));


-- -----------------------------------------------------
-- Table coprocenva.rol_briefcase
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "rol_briefcase" (
  rol_id INT NOT NULL,
  briefcase_id INT NOT NULL,
  INDEX fk_rol_briefcase_roles1_idx (rol_id ASC) VISIBLE,
  INDEX fk_rol_briefcase_briefcase1_idx (briefcase_id ASC) VISIBLE,
  CONSTRAINT fk_rol_briefcase_briefcase1
    FOREIGN KEY (briefcase_id)
    REFERENCES coprocenva.briefcase (briefcase_id),
  CONSTRAINT fk_rol_briefcase_roles1
    FOREIGN KEY (rol_id)
    REFERENCES coprocenva.roles (rol_id));


-- -----------------------------------------------------
-- Table coprocenva.rol_landings
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "rol_landings" (
  rol_id INT NOT NULL,
  landing_id INT NOT NULL,
  INDEX fk_rol_landings_roles1_idx (rol_id ASC) VISIBLE,
  INDEX fk_rol_landings_landings1_idx (landing_id ASC) VISIBLE,
  CONSTRAINT fk_rol_landings_landings1
    FOREIGN KEY (landing_id)
    REFERENCES coprocenva.landings (landing_id),
  CONSTRAINT fk_rol_landings_roles1
    FOREIGN KEY (rol_id)
    REFERENCES coprocenva.roles (rol_id));


-- -----------------------------------------------------
-- Table coprocenva.secondary_roles
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS "secondary_roles" (
  user_id INT NOT NULL,
  rol_id INT NOT NULL,
  INDEX fk_secondary_roles_users1_idx (user_id ASC) VISIBLE,
  CONSTRAINT fk_secondary_roles_users1
    FOREIGN KEY (user_id)
    REFERENCES coprocenva.users (user_id));
